#!/usr/bin/env python3
import atexit
import iot.dan
from flask import Flask, render_template, request
from fluent import sender
from fluent import event

#HOST = '0.0.0.0'
HOST = 'your_ip' #remember to change it
PORT = 7796
app = Flask(__name__)
connect_df = []

def on_data(*args):
    print('data:', args)
    sensor = args[0][:-7]
    idf = sensor+'_I'
    if sensor == 'Acceleration' or sensor == 'Gyroscope' or sensor == 'Magnetometer' or sensor == 'Orientation':
        event.Event('loggertest', {
            'from': idf,
            'to':   sensor,
            'data': args,
            'sample':[args[1][0][0],args[1][0][1],args[1][0][2]],
            'timestamp': args[1][0][3]
        })
    if sensor == 'Alcohol' or sensor == 'Humidity' or sensor == 'UV':
        event.Event('loggertest', {
            'from': idf,
            'to':   sensor,
            'data': args,
            'sample':[args[1][0][0]],
            'timestamp': args[1][0][1]
        })
    return True


def on_signal(signal, df_list):
    global connect_df
    print('signal:', signal, df_list)
    if 'CONNECT' == signal:
        for df in df_list:
            if df not in connect_df:
                connect_df.extend(df_list)
                connect_df = sorted(connect_df)
    elif 'DISCONNECT' == signal:
        for df in df_list:
            connect_df.remove(df)
    elif 'SUSPEND' == signal:
        # Not use
        pass
    elif 'RESUME' == signal:
        # Not use
        pass
    return True


def on_exit():
    iot.dan.deregister()


#@app.route('/')
#def mail_page():
#    global connect_df
#    return render_template('index.html', connect_df=connect_df)


@app.route('/<df>', methods=['POST'])
def get_push(df):
    data = request.json
    print(data)
    iot.dan.push(df, data)
    return '', 200

if '__main__' == __name__:
    odf_list = []
    odf_list.append(['Acceleration_logger', ['num']])
    odf_list.append(['Orientation_logger', ['num']])
    odf_list.append(['Gyroscope_logger', ['num']])
    odf_list.append(['Magnetometer_logger', ['num']])
    odf_list.append(['Alcohol_logger', ['num']])
    odf_list.append(['Humidity_logger', ['num']])
    odf_list.append(['UV_logger', ['num']])


    context = iot.dan.register(
        'https://your_domain_name/iottalk/csm',#remember to change it
        on_signal=on_signal,
        on_data=on_data,
        accept_protos=['mqtt'],
        odf_list=odf_list,
        name='Logger-ALL',
        profile={'model': 'FileLogger1'}
    )
    sender.setup('fluentd.test', host='localhost', port=24224)
    atexit.register(on_exit)

    app.run(
        host=HOST,
        port=PORT,
        threaded = True,
    )
