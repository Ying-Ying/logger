'''
This module implements a set of Connection Managers.

Connection manager provides a in-memery loopup table for connections.
'''
import json
import logging
import six

from abc import ABCMeta, abstractmethod
from functools import partial
from six import string_types
from six.moves import UserDict
from threading import Lock
from time import sleep
from uuid import UUID

from paho.mqtt import client as mqtt
from tornado.httpclient import HTTPClient, HTTPError
from zope import event as zevent

from iot import events
from iot.config import config
from iot.utils import SimpleLock, suppress
from iot.utils.mqtt import mqtt_json_payload, mqtt_json_pub

try:
    from collections.abc import ItemsView
except ImportError:
    from collections import ItemsView

log = logging.getLogger('iottalk.conn')


class DataConnMap(UserDict):
    '''
    The dictionary store all data connections

    If we request a non-exist key,
    the connection container will be auto-created.

    >>> data_conn = DataConnMap()
    >>> data_conn['meow']
    <Feature Conn Container: meow>
    >>> data_conn['meow'].i
    >>> data_conn['meow'].o

    >>> data_conn.get('meow')
    <Feature Conn Container: meow>

    >>> data_conn.get('acce', None) is None
    True

    >>> 'meow' in data_conn
    True
    >>> 'acce' in data_conn
    False
    '''

    class Feature(object):
        i = None  # input
        o = None  # output

        def __init__(self, name):
            self.name = name

        def __repr__(self):
            return '<Feature Conn Container: {}>'.format(self.name)

    def get(self, key, default=None):
        '''
        :type key: str
        '''
        if not isinstance(key, string_types):
            return default
        elif not key in self:
            return default
        return self[key]

    def __missing__(self, key):
        ret = self.Feature(key)
        self.data[key] = ret
        return ret


class Conn(object):
    '''
    The connection class used in ``ConnMap``

    :param id_: the ``UUID`` object

    >>> id_ = UUID('a54b5bf9-c39b-4248-b611-80d1ed4a36df')
    >>> c = Conn(id_)
    >>> c.id
    UUID('a54b5bf9-c39b-4248-b611-80d1ed4a36df')

    >>> # data channel is here
    >>> c.data
    {}
    >>> c.data['meow']  # feature auto-created
    <Feature Conn Container: meow>

    >>> # control channel is here
    >>> isinstance(c.ctrl, CtrlConn)
    True

    >>> c.conf
    {}
    '''

    def __init__(self, id_):
        self.id = id_
        self.ctrl = CtrlConn(id_=id_)
        self.data = DataConnMap()
        self.conf = {}


class CtrlConn(object):
    '''
    The control channel connection object.

    :param id_: the ``UUID`` object
    :param pub: the publish function with the spec same as
                ``paho.mqtt.client.Client.publish``.
    :param sub: the subscribe helper function. It must have a function
                attribute ``remove_callback`` in order to reset callback.
    :param rev: the ``revision`` token for checking race condiction.

    >>> def pub(*args):
    ...     pass

    >>> def sub(*args):
    ...     pass

    >>> # sub function must have ``remove_callbacks``
    >>> sub.remove_callback = (lambda : 42)

    >>> id_ = UUID('a54b5bf9-c39b-4248-b611-80d1ed4a36df')

    >>> cc = CtrlConn(id_, pub, sub, rev=42)
    >>> cc.pub is pub
    True
    >>> cc.sub is sub
    True
    >>> assert cc.rev == 42
    '''

    def __init__(self, id_, pub=None, sub=None, rev=None):
        self.id = id_
        self.pub = pub
        self.sub = sub
        self.da_ready = False
        self.da_ready_lock = Lock()
        self.res_callbacks = {}  # {'msg_id': (on_success, on_error)}
        self.rev = rev

    @property
    def sub(self):
        return getattr(self, '_sub', None)

    @sub.setter
    def sub(self, val):
        if val is None:
            return
        if getattr(self, '_sub', None):
            self._sub.remove_callback()

        self._sub = val
        self._sub(self._on_message)

    @mqtt_json_payload(raise_error=False)
    def _on_message(self, client, userdata, msg):
        '''
        The subscribe message callback

        We accept json format.

        The device application should send MQTT retained messages when the
        connection status changed::

            {
                'state': 'online|offline|broken',
                'rev': 'the rev field from registration',
            }

        * ``online``: should be sent right after the device application
          connects to the MQTT server

        * ``offline``: should be sent before the device application disconnects
          from the MQTT server

        * ``broken``: should be sent as the "Last Will Message" right after the
          device application connects to the MQTT server

        * ``rev``: this field is used for stripping out out-of-date message.

        For the response of other command messages, please checkout
        ``iot.csm.ctrl``
        '''
        id_ = msg.topic.split('/')[0]
        data = msg.payload

        log.info('connmgr on_mesaage %s: %s', id_, data)

        # check revision
        if data['state'] in ('online', 'offline', 'broken'):
            rev = data.get('rev')
            if self.rev != rev:
                log.warning(
                    'Device application %s '
                    'state msg revision unknown or out-of-date, dropping',
                    id_)
                return

        if data['state'] == 'online':
            with self.da_ready_lock:
                if self.da_ready is False:  # only emit on state change
                    zevent.notify(events.DevAppReady(self.id))

                self.da_ready = True
        elif data['state'] in ('offline', 'broken'):
            with self.da_ready_lock:
                if self.da_ready is True:  # only emit on state change
                    zevent.notify(events.DevAppOffline(self.id))

                self.da_ready = False
                # clean up
                self.res_callbacks.clear()
        else:
            with suppress(ValueError):
                self._handle_cmd_response(data)

        log.info('Device application %s state: %s', id_, data['state'])

        if data['state'] == 'broken':
            try:
                http_client = HTTPClient()
                http_client.fetch(
                    'http://localhost:9992/{}'.format(id_),
                    method='DELETE',
                    headers={'Content-Type': 'application/json'},
                    body=json.dumps({'rev': data['rev']}),
                    allow_nonstandard_methods=True
                )
            except HTTPError as e:
                pass
            finally:
                http_client.close()

    def _handle_cmd_response(self, response):
        '''
        The handler for command message response

        It should follow the structure described in
        :py:meth:`iot.mqtt.ctrl`.
        '''
        for field in ('msg_id', 'state'):
            if field not in response:
                msg = ('Invalid response, missing `msg_id` or `state` field. '
                       'Dropping {!r}'.format(response))
                log.error(msg)
                raise ValueError(msg)

        state = response['state']
        if state not in ('ok', 'error'):
            msg = 'Invalid state: {!r}'.format(state)
            log.error(msg)
            raise ValueError(msg)

        msg_id = response['msg_id']
        if msg_id not in self.res_callbacks:
            return

        success, error = self.res_callbacks.pop(msg_id)
        if state == 'ok':
            success(response)
        elif state == 'error':
            error(response)

    def add_res_callback(self, msg_id, on_success, on_error):
        '''
        Add a one-time response redirection.

        We will check the ``msg_id`` in the response package. If it is
        malformed, we will drop it.

        :param msg_id: the message id to matching the response.
        :param on_success: the callback function on success,
            a ``payload`` argument will be passed to it during the invocation.
        :param on_error: the callback function on error,
            a ``payload`` argument will be passed to it during the invocation.
        '''
        if msg_id in self.res_callbacks:
            log.warning(
                'Message id %r already in callback queue. Ignore', msg_id)
            return

        self.res_callbacks[msg_id] = (on_success, on_error)


class ConnMap(UserDict):
    '''
    The mapping uses ``uuid.UUID`` as key.

    Structure::

        {
            UUID('...'): {
                'ctrl': (<publish function>, <subscribe function>),
                'data': {
                    'feature': {
                        'i': <connection object>,
                        'o': <connection object>,
                    }.
                },
                'conf': {
                    'ctrl': {
                        'scheme': 'mqtt',
                        'host': 'localhost',
                        ...
                    },
                    'feature': {
                        ...
                    },
                },
            },
            ...
        }
    '''

    def confs(self):
        '''
        :return: a ``ItemsView`` for ``(uuid, conf)`` pairs.
        '''
        raise NotImplemented

    def data_channs(self):
        '''
        :return: a ``ItemsView`` for ``(uuid, data_channs)`` pairs.
        '''

    def ctrl_channs(self):
        '''
        :return: a ``ItemsView`` for ``(uuid, ctrl_channs)`` pairs.
        '''

    def get(self, key, default=None):
        '''
        :type key: ``UUID``
        '''
        if not isinstance(key, UUID):
            return default
        return self[key]

    def __getitem__(self, key):
        '''
        :param key: we accept key as an ``UUID``.
        :raise TypeError: if key is not an ``UUID`` object.
        '''
        if not isinstance(key, UUID):
            raise TypeError(
                'key should be an UUID object, not {!r}'.format(type(key)))

        if key not in self.data:
            return self.__missing__(key)
        return self.data[key]

    def __missing__(self, key):
        '''
        Auto create the ``Conn`` object
        '''
        ret = Conn(key)
        self.data[key] = ret
        return ret


class ConnMgrBase(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self.__conns = ConnMap()
        super(ConnMgrBase, self).__init__()

    def __del__(self):
        with suppress(AttributeError):
            super(ConnMgrBase, self).__del__()

    @property
    def conns(self):
        '''
        Get all managed connections
        '''
        return self.__conns

    @abstractmethod
    def setup_conn(self, proto, conn_conf):
        '''
        Create a connect which will be managed by connection manager
        '''


class MQTTConnMgrMixin(object):
    '''
    Connection Manager MQTT Mixin

    .. todo::
        - reconnect
    '''

    _mqtt_c = None

    def __init__(self):
        super(MQTTConnMgrMixin, self).__init__()
        self.mqtt_connected = False
        self.mqtt_conn_prev = False  # connection prev state
        self.subscribed_topic = []  # [(topic, qos), ...]

    def __del__(self):
        with suppress(AttributeError):
            super(MQTTConnMgrMixin, self).__del__()

        if self.mqtt_c is not None:
            self.mqtt_c.disconnect()
            self.mqtt_c.loop_stop()

    @property
    def mqtt_c(self):
        '''
        The ``paho.mqtt.client.Client`` instance
        '''
        if self._mqtt_c is None:
            raise AttributeError(
                'Please create client via ``setup_mqtt_conn`` first.')
        return self._mqtt_c

    def setup_mqtt_conn(self, *args, **kwargs):
        '''
        Setup a mqtt connection and manage it.

        For the available parameters, please check out
        :py:class:`paho.mqtt.client.Client`

        A var ``self._mqtt_c`` will be setup.

        This call will block until the mqtt server connected.
        '''
        self._mqtt_c_lock = SimpleLock()
        self._mqtt_c_lock.acquire()

        client = mqtt.Client(client_id='connmgr-{}'.format(config.session_id))
        client.on_connect = self.mqtt_on_connect
        client.on_disconnect = self.mqtt_on_disconnect
        client.on_subscribe = self.mqtt_on_subscribe

        for i in range(5):
            try:
                client.connect(*args, **kwargs)
            except:
                log.warning('mqtt connection retry after 1 sec')
                if i == 4:
                    raise
                sleep(1)
            else:
                break

        client.loop_start()

        self._mqtt_c = client
        if not self._mqtt_c_lock.acquire(timeout=10):
            raise RuntimeError('Connect to mqtt server timeout')
        del self._mqtt_c_lock

    def mqtt_on_connect(self, client, userdata, flags, rc):
        '''
        The MQTT client connect callback
        '''
        if rc != 0:
            if hasattr(self, '_mqtt_c_lock'):
                # we do not have _mqtt_c_lock if reconnecting
                self._mqtt_c_lock.release()
            msg = 'MQTT server connection refused, code {}'.format(rc)
            log.error(msg)
            raise IOError(msg)

        log.info('Connect to mqtt server successfully')

        if self.mqtt_conn_prev:  # in case of reconnection
            for topic, qos in self.subscribed_topic:
                log.info('Renew subscriptions %s', topic)
                self.mqtt_c.subscribe(topic, qos=qos)

        self.mqtt_connected = True

        if hasattr(self, '_mqtt_c_lock'):
            # we do not have _mqtt_c_lock if reconnecting
            self._mqtt_c_lock.release()

    def mqtt_on_disconnect(self, client, userdata, rc):
        '''
        The MQTT client disconnect callback
        '''
        log.info('MQTT server disconnected')
        self.mqtt_conn_prev = self.mqtt_connected
        self.mqtt_connected = False

    def mqtt_on_subscribe(self, client, userdata, mid, granted_qos):
        '''
        :param mid: the message id returned by ``Client.subscribe``
        '''
        pass

    def mqtt_sub(self, id_, feature, topic, qos=2):
        '''
        Subscribe to a mqtt topic.

        :param id_: the ``UUID`` object
        :param feature: the feature name, if feature name is ``_ctrl``, we will
                        setup the control channel.
        :param topic: the mqtt topic, e.g: ``uuid/feature/i``.
        :param qos: the mqtt qos level, should be 0, 1, 2
        :param ctrl: control channel or not
        :return: a function accept a *on message* callback.
                 This returned function also has following
                 function attributes:

                 :func.cb: the using callback will be stored here.
                 :func.client: the ``paho.mqtt.client.Client`` instance
                 :func.feature: the feature name related to the subscription
                 :func.id: the ``UUID`` object
                 :func.remove_callback: the function for unsubscribing topic
                 :func.topic: the mqtt topic
                 :func.qos: qos level
        '''
        def f(callback):
            if f.cb is not None:
                f.client.message_callback_remove(f.topic)
            f.client.message_callback_add(f.topic, callback)
            f.cb = callback
            log.info('connmgr msg callback setup %s', f.id)
            return callback

        f.cb = None
        f.client = self.mqtt_c
        f.feature = feature
        f.id = id_
        f.topic = topic
        f.qos = qos
        f.remove_callback = partial(
            self.mqtt_c.message_callback_remove, f.topic)

        self.mqtt_c.subscribe(topic, qos=qos)
        log.info('conn manager subscribe %s', topic)
        self.subscribed_topic.append((topic, qos))

        if feature != '_ctrl':
            self.conns[id_].data[feature].i = f
        else:
            self.conns[id_].ctrl.sub = f

        return f

    def mqtt_pub(self, id_, feature, topic, qos=2):
        '''
        Add a publish function to ``self.conns``.

        :param id_: the ``UUID`` object
        :param feature: the feature name, if feature name is ``_ctrl``, we will
                        setup the control channel
        :param ctrl: control channel or not
        :type topic: str

        :return: the ``publish`` function
        '''
        if feature != '_ctrl':
            pub = partial(self.mqtt_c.publish, topic, qos=qos)
            self.conns[id_].data[feature].o = pub
        else:
            pub = partial(mqtt_json_pub(self.mqtt_c.publish), topic, qos=qos)
            self.conns[id_].ctrl.pub = pub

        return pub

    def mqtt_ctrl(self, id_, pub_topic, sub_topic, rev):
        '''
        :param id_: the ``UUID`` object
        :param pub_topic:
        :param sub_topic:
        :param rev:
        '''
        pub = self.mqtt_pub(id_, '_ctrl', pub_topic)
        sub = self.mqtt_sub(id_, '_ctrl', sub_topic)
        self.conns[id_].ctrl.rev = rev
        return pub, sub


class WSConnMgrMixin(object):
    '''
    Connection Manager WebSocket Mixin
    '''

    def __init__(self):
        super(WSConnMgrMixin, self).__init__()

    def __del__(self):
        with suppress(AttributeError):
            super(WSConnMgrMixin, self).__del__()

    def setup_ws_conn(self, conf):
        '''
        Setup a websocket connection and manage it.

        :param conf: TBD
        '''
        raise NotImplementedError()


class IoTConnMgr(ConnMgrBase, MQTTConnMgrMixin, WSConnMgrMixin):
    '''
    IoTtalk Connection Manager
    '''

    def __init__(self):
        '''
        .. todo::
            - make ``keepalive`` configurable
        '''
        super(IoTConnMgr, self).__init__()

        self.setup_conn(
            'mqtt', config.mqtt_conf['host'],
            port=config.mqtt_conf['port'], keepalive=60)

    def __del__(self):
        with suppress(AttributeError):
            super(IoTConnMgr, self).__del__()

    def setup_conn(self, proto, *args, **kwargs):
        '''
        Create a connect which will be managed by connection manager

        :param proto: ``mqtt`` or ``websocket``
        :param conn_conf: a dict represent the config of connection
        '''
        proto = proto.lower()

        if proto == 'mqtt':
            self.setup_mqtt_conn(*args, **kwargs)
        elif proto == 'websocket':
            self.setup_ws_conn(*args, **kwargs)
        else:
            raise NotImplementedError(
                'Protocol {!r} not supported'.format(proto))


# make a singleton
iot_conn_mgr = IoTConnMgr()
