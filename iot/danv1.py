"""Support V1 API."""
import json
import random
import signal

from threading import Lock

from iot.dan import Client

ENDPOINT = None

profile = {
    'd_name': None,
    'dm_name': None,  # 'MorSensor'
    'is_sim': False,
    'df_list': [],  # ['Acceleration', 'Temperature']
}


class Client_v1(Client):

    def __init__(self):
        super().__init__()
        self.ccm_sub_topic = 'iottalk/api/gui/res/{}'
        self.ccm_pub_topic = 'iottalk/api/gui/req/{}'
        self.alias_name_lock = Lock()
        self.alias_name_lock.acquire()
        self.ccm_online_lock = Lock()
        self.ccm_online_lock.acquire()
        self.alias_name = {}

    def _on_connect(self, client, userdata, flags, rc):
        self.ccm_sub_topic = self.ccm_sub_topic.format(self.context.app_id)
        self.ccm_pub_topic = self.ccm_pub_topic.format(self.context.app_id)
        client.subscribe(self.ccm_sub_topic)
        client.message_callback_add(self.ccm_sub_topic,
                                    self._on_ccm_message)
        client.publish(self.ccm_pub_topic, json.dumps({'op': 'attach'}))
        self.ccm_online_lock.release()
        super()._on_connect(client, userdata, flags, rc)

    def _on_ccm_message(self, client, userdata, msg):
        payload = json.loads(msg.payload.decode())

        if 'op' not in payload:
            pass
        elif 'anno' == payload['op']:
            if 'state' in payload and 'Authentication Fail' == payload['state']:
                client.publish(self.ccm_pub_topic, json.dumps({'op': 'attach'}))
        elif payload['op'] in ['get_alias_name', 'set_alias_name']:
            data = payload['data']
            if 'alias_name' in data:
                self.alias_name[data['df_name']] = data['alias_name']
            else:
                self.alias_name[data['df_name']] = data['df_name']
            self.alias_name_lock.release()

    def get_alias(self, df_name):
        self.alias_name_lock.acquire()
        self.ccm_online_lock.acquire()
        payload = {
            'op': 'get_alias_name',
            'data': {
                'mac_addr': str(_default_client.context.app_id),
                'df_name': df_name
            }
        }
        self.context.mqtt_client.publish(self.ccm_pub_topic,
                                         json.dumps(payload))
        self.ccm_online_lock.release()

    def set_alias(self, df_name, alias_name):
        self.alias_name_lock.acquire()
        self.ccm_online_lock.acquire()
        payload = {
            'op': 'set_alias_name',
            'data': {
                'mac_addr': str(_default_client.context.app_id),
                'df_name': df_name,
                'alias_name': alias_name
            }
        }
        self.context.mqtt_client.publish(self.ccm_pub_topic,
                                         json.dumps(payload))
        self.ccm_online_lock.release()


def _signal_handler(sig, frame):
    print('alias name time out.')
    if _default_client.alias_name_lock.locked():
        _default_client.alias_name_lock.release()


def _on_signal(signal, df_list):
    return True


def _on_data(df_name, data):
    try:
        iter(data)
        _data_cache[df_name] = data
    except:
        _data_cache[df_name] = [data]


def push(idf, data, **kwargs):
    return _default_client.push(idf, data, **kwargs)


def pull(df_name):
    return _data_cache.pop(df_name) if df_name in _data_cache else None


def get_alias(df_name):
    _default_client.alias_name_lock.release()
    _default_client.get_alias(df_name)
    signal.alarm(3)
    _default_client.alias_name_lock.acquire()
    signal.alarm(0)
    return (_default_client.alias_name[df_name]
            if df_name in _default_client.alias_name else df_name)


def set_alias(df_name, alias_name):
    _default_client.alias_name_lock.release()
    _default_client.set_alias(df_name, alias_name)
    signal.alarm(3)
    _default_client.alias_name_lock.acquire()
    signal.alarm(0)
    return True


def register_device(id_):
    if profile['d_name'] is None:
        profile['d_name'] = '{}.{}'.format(random.uniform(1, 100), profile['dm_name'])

    return _default_client.register(ENDPOINT,
                                    on_signal=_on_signal,
                                    on_data=_on_data,
                                    id_=id_,
                                    name=profile['d_name'],
                                    idf_list=profile['df_list'],
                                    odf_list=profile['df_list'],
                                    accept_protos=['mqtt'],
                                    profile={'model': profile['dm_name']})


def device_registration_with_retry(url=None, id_=None):
    if not url:
        url = ENDPOINT

    if profile['d_name'] is None:
        profile['d_name'] = '{}.{}'.format(random.uniform(1, 100), profile['dm_name'])

    return _default_client.register(url,
                                    on_signal=_on_signal,
                                    on_data=_on_data,
                                    id_=id_,
                                    name=profile['d_name'],
                                    idf_list=profile['df_list'],
                                    odf_list=profile['df_list'],
                                    accept_protos=['mqtt'],
                                    profile={'model': profile['dm_name']})

_default_client = Client_v1()
_data_cache = {}
signal.signal(signal.SIGALRM, _signal_handler)
