import json
import logging

from functools import wraps

from paho.mqtt.client import Client

log = logging.getLogger('iottalk.utils.mqtt')


def mqtt_json_pub(func=None, raise_error=True):
    '''
    Decorator function for mqtt json publish (``paho.mqtt.client.publish``)

    :param raise_error: if true, we will throw exception; if false, we just log
                        it.
    :raise: TypeError if json encode failed or payload is not a list, a dict
                      or a tuple.
    '''
    def decorator(func):
        @wraps(func)
        def wrapper(topic, payload, **kwargs):
            try:
                if not isinstance(payload, (list, tuple, dict)):
                    raise TypeError()

                payload = json.dumps(payload)
            except TypeError:
                msg = 'Json encode failed on payload: {!r}.'.format(payload)
                log.warning(msg)
                if raise_error:
                    raise TypeError(msg)
            else:
                return func(topic, payload, **kwargs)

        return wrapper

    if func:
        return decorator(func)
    return decorator


def mqtt_json_payload(func=None, raise_error=True):
    '''
    Decorator helper to decode json message
    for ``paho.mqtt.client.on_message`` callback.

    :param raise_error: if true, we will throw exception; if false, we just log
                        it.
    :raise: TypeError if decode failed
    '''
    def decorator(func):
        @wraps(func)
        def wrapper(*args):
            msg = args[-1]
            try:
                msg.payload = json.loads(msg.payload.decode('utf-8'))
                if not isinstance(msg.payload, (list, dict)):
                    raise TypeError
            except (TypeError, ValueError) as e:
                msg = ('Got malformed payload: {!r}, dropped.'
                       '{!r}').format(msg.payload, e)
                log.warning(msg)
                if raise_error:
                    raise
            else:
                func(*args)

        return wrapper

    if func:
        return decorator(func)
    return decorator


class JsonClient(Client):
    '''
    The client wrap message with json decode/encode
    '''

    def __init__(self, *args, **kwargs):
        super(type(self), self).__init__(*args, **kwargs)

        self._on_json_message = None
        self.publish_json = mqtt_json_pub(self.publish)

    @property
    def on_json_message(self):
        return self._on_json_message

    @on_json_message.setter
    def on_json_message(self, func):
        func = mqtt_json_payload(func, raise_error=False)
        self._on_json_message = func
        self.on_message = func
